<!-- Content Header (Page header) -->
<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark">Acta de Nacimiento</h1>
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
	<div class="container-fluid">
		<!-- Main row -->

		<div class="row">
			<div class="mensaje col-md-12">

			</div>
			<div class="col-md-4">
				<!-- select -->
				<div class="form-group">
					<!-- <label for="dpi">Persona</label>
					<select class="form-control" name="dpi" id="dpi">
						<option value="">Seleccione Uno</option>
					</select> -->
					<a href="javascript:printDiv('persona');" class="btn btn-default">Imprimir</a>
				</div>
			</div>
		</div>
		<div class="row printable" id="persona">

		</div>
		<!-- /.row (main row) -->
	</div><!-- /.container-fluid -->
</section>
<!-- /.content -->

<script>
function printDiv(divID) {
	//Get the HTML of div

	var divElements = document.getElementById(divID).innerHTML;

	//Get the HTML of whole page
	var oldPage = document.body.innerHTML;

	//Reset the pages HTML with divs HTML only

		document.body.innerHTML = 

		"<html><head><title></title></head><body>" + 
		divElements + "</body>";



	//Print Page
	window.print();

	//Restore orignal HTML
	document.body.innerHTML = oldPage;

}
$(function(){
		// llenarPersonas();
	
	$.ajax({
		url: '<?php echo base_url('api/persona/getPersona')?>',
		data: {dpi: <?php echo $this->session->userdata('dpi')?>},
		success: function (response) {
			$('#persona').empty();
			var innerHtml = 'Nombres :'+response.persona.nombres+'<br>';
			innerHtml += 'Apellidos :'+response.persona.apellidos+'<br>';
			innerHtml += 'DPI :'+response.persona.dpi+'<br>';
			innerHtml += 'Fecha de Nacimiento:'+response.persona.fecha_nacimiento+'<br>';
			innerHtml += 'Acta de Nacimiento:'+response.persona.acta+'<br>';
			innerHtml += 'Departamento:'+response.persona.dept+'<br>';
			innerHtml += 'Municipio:'+response.persona.muni+'<br>';

			$('#persona').append(innerHtml);
		}
	});
});
function llenarPersonas() {
	$.ajax({
		url: '<?php echo base_url('api/persona/getPersonas')?>',
		data: {difunto: 0},
		success: function (response) {
			$('#dpi').empty();
			$('#dpi').append(new Option("Seleccione Uno",""));
			$.each(response,function (index, value) {
				$('#dpi').append(new Option(value.nombre,value.dpi));
			});
		}
	});
}
$('#dpi').change(function(){
	$.ajax({
		url: '<?php echo base_url('api/persona/getPersona')?>',
		data: {dpi: <?php echo $this->session->userdata('dpi')?>},
		success: function (response) {
			$('#persona').empty();
			var innerHtml = 'Nombres :'+response.persona.nombres+'<br>';
			innerHtml += 'Apellidos :'+response.persona.apellidos+'<br>';
			innerHtml += 'DPI :'+response.persona.DPI_cui+'<br>';
			innerHtml += 'Fecha de Nacimiento:'+response.persona.fecha_nacimiento+'<br>';
			innerHtml += 'Acta de Nacimiento:'+response.persona.acta+'<br>';
			innerHtml += 'Departamento:'+response.persona.dept+'<br>';
			innerHtml += 'Municipio:'+response.persona.muni+'<br>';

			$('#persona').append(innerHtml);
		}
	});
});
</script>
