
			<!-- Content Header (Page header) -->
			<div class="content-header">
				<div class="container-fluid">
					<div class="row mb-2">
						<div class="col-sm-6">
							<h1 class="m-0 text-dark">Personas
								
							</h1>
						</div><!-- /.col -->
					</div><!-- /.row -->
				</div><!-- /.container-fluid -->
			</div>
			<!-- /.content-header -->

			<!-- Main content -->
			<section class="content">
				<div class="container-fluid">
					<!-- Main row -->
					<div class="row">
						
						<!-- /.card-header -->
						<div class="card-body">
							<table id="tablaPersonas" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>Nombre</th>
										<th>Fecha de Nacimiento</th>
										<th>DPI</th>
										<th>Departamento</th>
										<th>Municipio</th>
										<th class="no-sort" style="width: 10%"></th>
									</tr>
								</thead>
							</table>
						</div>
						<!-- /.card-body -->
					</div>
					<!-- /.row (main row) -->
				</div><!-- /.container-fluid -->
			</section>
			<!-- /.content -->

			<script type="text/javascript">
    $(function () {
        $('#tablaPersonas').DataTable({
			"pageLength" : 25,
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": '<?php echo base_url('interno/personas/obtenerPersonas')?>',
                "data": function (d) {
                }
            },
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "columnDefs": [ {
                "targets": 'no-sort',
                "orderable": false,
            } ],
            "language": {
                "lengthMenu": "Mostrando _MENU_ personas por pagina",
                "zeroRecords": "No se encontró nada - lo sentimos",
                "info": "Mostrando pagina _PAGE_ de _PAGES_",
                "infoEmpty": "No hay personas disponibles",
                "infoFiltered": "(filtrando de _MAX_ total personas)",
                "search":         "Buscar:",
                "paginate": {
                    "first":      "Primero",
                    "last":       "Ultimo",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                },
            },"columns": [
                null,
                null,
                null,
                null,
                null,
            ]
        });
    });

</script>
