<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require_once APPPATH . 'libraries/JWT.php';
use \Firebase\JWT\JWT;
class Login extends REST_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct() {
		parent::__construct();
		$this->load->model('persona_model','persona');
	}

	public function index_post()
	{   
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('session');
		$this->form_validation->set_rules('dpi', 'DPI', 'trim|required');
		$this->form_validation->set_rules('clave', 'Contraseña', 'trim|required');
		if ($this->form_validation->run() === false) {
			return $this->response( [
				'estado' => REST_Controller::HTTP_INTERNAL_SERVER_ERROR,
				'mensaje' => validation_errors()
			], REST_Controller::HTTP_OK );
		}else{
			$persona = $this->persona->checkLogin($this->input->post('dpi'),$this->input->post('clave'));
			if($persona == 0){
				return $this->response( [
					'estado' => REST_Controller::HTTP_INTERNAL_SERVER_ERROR,
					'mensaje' => "El usuario y contraseña no coinciden."
				], REST_Controller::HTTP_OK );
			}else{
				$date = new DateTime();
				$token = array(
					'dpi' => $this->input->get('dpi'),
					'iat' => $date->getTimestamp(),
					'exp' => $date->getTimestamp() + 60*60*5,
				);
				$output = array(
					'estado' => REST_Controller::HTTP_OK,
					'mensaje' => 'Inicio de sesion exitoso.',
					'token' => JWT::encode($token, "SA-12-2019"),
				);
				return $this->response($output, REST_Controller::HTTP_OK);
				// $this->response( $persona, REST_Controller::HTTP_OK );
			}
		}
		
		
	}

	public function setNuevaContrasena_post()
	{
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('session');
		$this->form_validation->set_rules('dpi', 'DPI', 'trim|required');
		$this->form_validation->set_rules('correo', 'Correo Electronico', 'trim|required');
		if ($this->form_validation->run() === false) {
			return $this->response( [
				'estado' => REST_Controller::HTTP_INTERNAL_SERVER_ERROR,
				'mensaje' => validation_errors()
			], REST_Controller::HTTP_OK );
        }else{
			$this->load->library('session');
			$this->load->library('email');
			$this->load->helper('form');
			$this->load->helper('string');
			$this->load->model('persona_model','persona');

			$from_email = "info@renap.com";
			$to_email = $this->input->post('correo');
			$dpi = $this->input->post('dpi');
			//Load email library
			$persona = $this->persona->getOneBy('dpi',$this->input->post('dpi'));
			if(!isset($persona->idPersona) || empty($persona)){
				return $this->response( [
					'estado' => REST_Controller::HTTP_INTERNAL_SERVER_ERROR,
					'mensaje' => "No se encontro la persona con el DPI: ".$this->input->post('dpi')
				], REST_Controller::HTTP_OK );
			}else{
				$password = random_string();
				$this->persona->update(['password' => md5($password)],$persona->idPersona,'idPersona');
				$this->email->from($from_email, 'RENAP');
				$this->email->to($to_email);
				$this->email->subject('Nueva Contraseña');
				$this->email->message("Tu nueva contraseña es: $password para el DPI: $dpi");
				//Send mail
				if($this->email->send()){
					$msg = "Tu nueva contraseña ha sido enviada correctamente.";
				}else{
					$msg = "Hubo un error al enviar la contraseña, intenta nuevamente.";
				}
				return $this->response( [
					'estado' => REST_Controller::HTTP_INTERNAL_SERVER_ERROR,
					'mensaje' => $msg
				], REST_Controller::HTTP_OK );
			}
		}
	}
}
