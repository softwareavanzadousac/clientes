<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

class Persona extends REST_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct() {
		parent::__construct();
		$this->load->model('persona_model','persona');
	}

	public function getPersona_get()
	{   
		$dpi = $this->input->get('dpi','');
		$persona = $this->persona->getPersona($dpi);
		$padre = $this->persona->getOneBy('dpi',$persona->dpipadre);
		$madre = $this->persona->getOneBy('dpi',$persona->dpimadre);
		$licencia = $this->persona->getLicencia($dpi);
		$result = array(
			'persona' => $persona,
			'padre' => $padre,
			'madre' => $madre,
			'licencia' => $licencia,
		);
		$this->response($result, REST_Controller::HTTP_OK);
	}


	public function getPersonas_get()
	{   
		$genero = $this->input->get('genero','');
		$estado_civil = $this->input->get('estado_civil','');
		$difunto = $this->input->get('difunto','');
		$personas = $this->persona->getPersonas($genero,$estado_civil,$difunto);
		$this->response($personas, REST_Controller::HTTP_OK);
	}


	public function getPersonasLicencia_get()
	{   
		$personas = $this->persona->getPersonasLicencia();
		$this->response($personas, REST_Controller::HTTP_OK);
	}

	public function getPersonaGeneracion_get()
	{   
		$dpi = $this->input->get('dpi','');
		$persona = $this->persona->getPersona($dpi);
		if(empty($persona)){
			return $this->response($persona, REST_Controller::HTTP_OK);
		}
		$hijos = $this->persona->getHijos($persona->dpi);
		$padres = $this->persona->getPadres($persona);
		$abuelos = array();
		$bisabuelos = array();
		$query = $this->db->last_query();
		foreach ($padres as $padre) {
			$abuelos = array_merge($abuelos,$this->persona->getPadres($padre));
		}
		foreach ($abuelos as $abuelo) {
			$bisabuelos = array_merge($bisabuelos,$this->persona->getPadres($abuelo));
		}
		$result = array(
			'persona' => $persona,
			'hijos' => $hijos,
			'padres' => $padres,
			'abuelos' => $abuelos,
			'bisabuelos' => $bisabuelos,
			'query' => $query,
		);
		$this->response($result, REST_Controller::HTTP_OK);
	}
}
