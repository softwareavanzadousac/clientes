<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

class Resource extends REST_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct() {
		parent::__construct();
		$this->load->model('resource_model','resource');
	}

	public function getDepartamentos_get()
	{   
		$departamentos = $this->resource->getDepartamentos();
		if(empty($departamentos)){
			$this->response( [
				'estado' => REST_Controller::HTTP_INTERNAL_SERVER_ERROR,
				'mensaje' => 'No se encontraron los departamentos'
			], REST_Controller::HTTP_NOT_FOUND );
		}else{
            $this->response($departamentos, REST_Controller::HTTP_OK);
			// $this->response( $persona, REST_Controller::HTTP_OK );
		}
	}

	
	public function getMunicipios_get()
	{   
		$municipios = $this->resource->getMunicipios($this->input->get('idDepartamento'));
		if(empty($municipios)){
			$this->response( [
				'estado' => REST_Controller::HTTP_INTERNAL_SERVER_ERROR,
				'mensaje' => 'No se encontraron los municipios'
			], REST_Controller::HTTP_NOT_FOUND );
		}else{
            $this->response($municipios, REST_Controller::HTTP_OK);
			// $this->response( $persona, REST_Controller::HTTP_OK );
		}
	}

}
