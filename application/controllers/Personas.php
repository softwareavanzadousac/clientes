<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Personas extends CI_Controller {


	function __construct() {
		parent::__construct();
		$this->load->model('persona_model','persona');
	}

	public function index()
	{
		$this->load->template('personas');
	}
	public function obtenerPersonas()
	{
		// Datatables Variables
		$draw = intval($this->input->get("draw"));
		$start = intval($this->input->get("start"));
		$length = intval($this->input->get("length"));

		$personas = $this->persona->getAll($start,$length);

		$data = array();

		foreach($personas->result() as $r) {

			 $data[] = array(
				  $r->nombre,
				  $r->fecha_nacimiento,
				  $r->dpi,
				  $r->departamento,
				  $r->municipio
			 );
		}

		$output = array(
			 "draw" => $draw,
			   "recordsTotal" => $this->persona->count(),
			   "recordsFiltered" => $this->persona->count(),
			   "data" => $data
		  );
		echo json_encode($output);
		exit();
	}
}
