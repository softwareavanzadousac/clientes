<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Licencia extends MY_Controller {


	function __construct() {
		parent::__construct();
	}

	public function index()
	{
		$this->load->templateprivate('private/licencia');
	}
}
