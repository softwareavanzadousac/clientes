<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'libraries/JWT.php';
use \Firebase\JWT\JWT;

class Login extends CI_Controller {


	function __construct() {
		parent::__construct();
	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		if($this->session->userdata('logged_in') == TRUE) {
    		redirect('externo/inicio');
        }
		$this->load->view('login');
	}

	public function iniciar()
	{
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('session');
		$this->load->model('persona_model','persona');
		$this->form_validation->set_rules('dpi', 'DPI', 'trim|required');
		$this->form_validation->set_rules('password', 'Contraseña', 'trim|required');
		if ($this->form_validation->run() === false) {
			$this->session->set_flashdata('error_msg',validation_errors());
			redirect('login');
		}else{
			$dpi = $this->input->post('dpi');
			$password = $this->input->post('password');
			$persona = $this->persona->checkLogin($dpi,$password);
			if($persona == 0){
				$this->session->set_flashdata('error_msg','El usuario y contraseña no coinciden.');
				redirect('login');
			}else{
				$date = new DateTime();
				$token = JWT::encode(
					array(
						'dpi' => $dpi,
						'iat' => $date->getTimestamp(),
						'exp' => $date->getTimestamp() + 60*60*5,
					), "SA-12-2019");
				$this->session->set_userdata(
					array(
						'logged_in' => true,
						'dpi' => $dpi,
						'token' => $token,
					)
				);
				$this->session->set_flashdata('success_msg','Inicio de sesion exitoso.');
				redirect('externo/inicio');
			}
		}
	}

	public function salir()
	{
		$this->session->set_userdata(array(
			'logged_in' => false,
			'dpi' => null,
			'token' => null,
		));
		$this->session->set_flashdata('success_msg', 'Saliste!');
        redirect('/');
	}

	public function forgot()
	{
		$this->load->library('session');
        $this->load->library('email');
		$this->load->helper('form');
		$this->load->helper('string');
		$this->load->model('persona_model','persona');

		$from_email = "info@renap.com";
        $to_email = $this->input->post('email');
        $dpi = $this->input->post('dpi');
		//Load email library
		$persona = $this->persona->getOneBy('dpi',$this->input->post('dpi'));
		if(!isset($persona->idPersona) || empty($persona)){
            $this->session->set_flashdata("error_msg","No se encontro la persona con el DPI: ".$this->input->post('dpi'));
			redirect('/');
		}else{
			$password = random_string();
			$this->persona->update(['password' => md5($password)],$persona->idPersona,'idPersona');
			$this->email->from($from_email, 'RENAP');
			$this->email->to($to_email);
			$this->email->subject('Nueva Contraseña');
			$this->email->message("Tu nueva contraseña es: $password para el DPI: $dpi");
			//Send mail
			if($this->email->send())
				$this->session->set_flashdata("success_msg","Congragulation Email Send Successfully.");
			else
				$this->session->set_flashdata("error_msg","You have encountered an error");
			$this->load->view('login');
		}

	}
}
