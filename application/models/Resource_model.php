<?php
class Resource_model extends MY_Model
{
    protected $table = 'Persona';

    public function __construct()
    {
        parent::__construct();
	}
	
	public function getDepartamentos()
	{
		return $this->db->from('Departamento')->get()->result();
	}

	public function getMunicipios($departamento_id = '')
	{
		if($departamento_id == ''){
			return array();
		}
		return $this->db->select('idMunicipio, nombre')->from('Municipio')->where('departamento_id',$departamento_id)->get()->result();
	}
}

/*

+-------------+--------------+------+-----+---------+----------------+
| Field       | Type         | Null | Key | Default | Extra          |
+-------------+--------------+------+-----+---------+----------------+
| ID          | int(11)      | NO   | PRI | NULL    | auto_increment |
| AREA_TITULO | varchar(150) | NO   |     | NULL    |                |
| AREA_CLAVE  | varchar(150) | YES  |     | NULL    |                |
| AREA_STATUS | tinyint(1)   | YES  |     | 1       |                |
+-------------+--------------+------+-----+---------+----------------+

 */
