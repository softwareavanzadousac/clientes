<?php
class Persona_model extends MY_Model
{
    protected $table = 'Persona';

    public function __construct()
    {
        parent::__construct();
	}
	
	public function checkLogin($dpi, $password)
	{
		if($dpi == '' || $password == ''){
			return 0;
		}
		return $this->db->from($this->table)->where('dpi',$dpi)->where('password',md5($password))->count_all_results();
	}

	public function count()
	{
		return $this->db->from($this->table)->count_all_results();
	}

	public function getAll($start = 0, $length = 25)
	{
		return $this->db->from($this->table.' p')
						->select("concat(p.nombres,' ',p.apellidos) nombre,p.fecha_nacimiento,p.dpi dpi, m.nombre municipio, d.nombre departamento",false)
						->join('Municipio m','m.idMunicipio = p.municipio_id')
						->join('Departamento d','d.idDepartamento = m.departamento_id')
						->limit($length,$start)->get();
	}

	public function getPersonas($genero = '',$estado_civil = '',$difunto = '')
	{
		$personas = $this->db->select('dpi as dpi, concat(nombres, " ", apellidos) as nombre',false)
							->from($this->table);
		if($genero != ''){
			$personas = $personas->where('genero',$genero);
		}
		if($estado_civil != ''){
			$personas = $personas->where_in('estadocivil',$estado_civil);
		}
		if($difunto != ''){
			$personas = $personas->where('difunto',$difunto);
		}			
							
		return $personas->order_by('nombres')->where('TIMESTAMPDIFF(YEAR,fecha_nacimiento,CURDATE()) > 18')->get()->result_array();
	}

	public function getPersonasLicencia()
	{
		$personas = $this->db->select('Persona.dpi as dpi, concat(nombres, " ", apellidos) as nombre',false)
							->from($this->table)
							->join('Licencia','Licencia.dpi = Persona.dpi');				
		return $personas->order_by('nombres')->get()->result_array();
	}

	public function getLicencia($dpi = '')
	{
		if($dpi == ''){
			return array();
		}
		return $this->db->select('tipo, anios',false)->from('Licencia')->where('dpi',$dpi)->get()->row();
	}

	public function getHijos($dpi = '')
	{
		if($dpi == ''){
			return array();
		}
		return $this->db->from($this->table)
						->where('dpipadre',$dpi)
						->or_where('dpimadre',$dpi)
						->get()
						->result(); 
	}

	public function getPadres($persona)
	{
		if(!isset($persona->dpipadre) && !isset($persona->dpimadre)){
			return array();
		}
		$query = $this->db->from($this->table);
		if(isset($persona->dpipadre) && isset($persona->dpimadre)){
			$query = $query->where("(dpi = $persona->dpipadre OR dpi = $persona->dpimadre)");
		}elseif(isset($persona->dpipadre)){
			$query = $query->where("dpi = $persona->dpipadre");
		}elseif(isset($persona->dpimadre)){
			$query = $query->where("dpi = $persona->dpimadre");
		}
		
		return $query->get()->result();
	}

	public function getPersona($dpi = '')
	{
		if($dpi == ''){
			return array();
		}
		return $this->db->select('p.*, m.nombre muni, d.nombre dept')
						->from('Persona p')
						->join('Municipio m','p.municipio = m.llave')
						->join('Departamento d', 'p.departamento = d.llave')
						->where('dpi',$dpi)
						->get()
						->row();
	}
}

/*

+-------------+--------------+------+-----+---------+----------------+
| Field       | Type         | Null | Key | Default | Extra          |
+-------------+--------------+------+-----+---------+----------------+
| ID          | int(11)      | NO   | PRI | NULL    | auto_increment |
| AREA_TITULO | varchar(150) | NO   |     | NULL    |                |
| AREA_CLAVE  | varchar(150) | YES  |     | NULL    |                |
| AREA_STATUS | tinyint(1)   | YES  |     | 1       |                |
+-------------+--------------+------+-----+---------+----------------+

 */
